import { Response } from 'supertest';

export interface SubstitutionResponse extends Response {
    body: {
        data: number;
        error: string;
    };
}