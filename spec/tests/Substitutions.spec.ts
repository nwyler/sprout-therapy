import StatusCodes from 'http-status-codes';
import { SuperTest, Test } from 'supertest';
import {pErr} from '../../src/shared/functions'
import { SubstitutionResponse } from '../support/types';
import app from '../../src/Server';
import supertest from 'supertest';

describe('Substitution', () => {

    const basePath = '/api';
    const substitutionPath = `${basePath}/substitution`;

    const { BAD_REQUEST, OK } = StatusCodes;

    describe(`"GET:${substitutionPath}"`, () => {

        it(`should return a JSON object with a Number and a status code of "${OK}" if the
            request was successful.`, (done) => {
            supertest(app).get(substitutionPath)
            .query({ A:true, B:true, C:true, D:1.1, E:10, F:8 })
                .end((err: Error, res: SubstitutionResponse) => {
                    pErr(err);
                    expect(res.status).toBe(OK);
                    expect(res.body.data).toEqual(jasmine.any(Number));
                    expect(Number(res.body.data) === res.body.data && Number(res.body.data) % 1 !== 0).toBe(true, 'D must be a Decimal number');
                    expect(res.body.error).toBeUndefined();
                    done();
                });
        });

        it(`should return a JSON object containing an error message and a status code of
            "${BAD_REQUEST}" if the request was unsuccessful.`, (done) => {
            const errMsg = 'B and one of (A or C) to be true';

            supertest(app).get(substitutionPath)
                .query({ A:false, B:false, C:false, D:1.1, E:10, F:8 })
                .end((err: Error, res: SubstitutionResponse) => {

                    pErr(err);
                    expect(res.status).toBe(BAD_REQUEST);
                    expect(res.body.error).toBe(errMsg);
                    done();
                });
        });
    });
});


describe('Custom_1', () => {

    const basePath = '/api';
    const customPath = `${basePath}/custom_1`;

    const { BAD_REQUEST, OK } = StatusCodes;

    describe(`"GET:${customPath}"`, () => {

        it(`should return a JSON object with a Number and a status code of "${OK}" if the
            request was successful.`, (done) => {
            supertest(app).get(customPath)
            .query({ A:true, B:true, C:true, D:1.2, E:10, F:8 })
                .end((err: Error, res: SubstitutionResponse) => {
                    pErr(err);
                    expect(res.status).toBe(OK);
                    expect(res.body.data).toEqual(jasmine.any(Number));
                    expect(Number(res.body.data) === res.body.data && res.body.data % 1 !== 0).toEqual(true);
                    expect(res.body.error).toBeUndefined();
                    done();
                });
        });

        it(`should return a JSON object containing an error message and a status code of
            "${BAD_REQUEST}" if the request was unsuccessful.`, (done) => {
            const errMsg = 'B and one of (A or C) to be true';

            supertest(app).get(customPath)
                .query({ A:true, B:false, C:true, D:1.2, E:10, F:8 })
                .end((err: Error, res: SubstitutionResponse) => {
                    pErr(err);
                    expect(res.status).toBe(BAD_REQUEST);
                    expect(res.body.error).toBe(errMsg);
                    done();
                });
        });
    });
});


describe('Custom_2', () => {

    const basePath = '/api';
    const customPath = `${basePath}/custom_2`;

    const { BAD_REQUEST, OK } = StatusCodes;

    describe(`"GET:${customPath}"`, () => {

        it(`should return a JSON object with a Number and a status code of "${OK}" if the
            request was successful.`, (done) => {
            supertest(app).get(customPath)
            .query({ A:true, B:true, C:true, D:1.2, E:0, F:0 })
                .end((err: Error, res: SubstitutionResponse) => {
                    pErr(err);
                    expect(res.status).toBe(OK);
                    expect(res.body.data).toEqual(jasmine.any(Number));
                    expect(Number(res.body.data) === res.body.data && res.body.data % 1 !== 0).toEqual(true);
                    expect(res.body.error).toBeUndefined();
                    done();
                });
        });
    });
});
