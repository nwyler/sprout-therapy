# README #

# Sprout Therapy Assignment #

I decided to use **Node** with **Typescript** and create an API based on an **Express Server**.  
I'm exposing three ***endpoints** to get the different substitutions:

* /substitution
* /custom_1
* /custom_2

To achieve this in a scalable way and reusing as much code as possible I decided to create a base class called Substitution (/models/Substitution_model.ts).  
This class receives all the parameters and has one **get method** called **H** that returns the corresponding substitution.  
All the class variables are **read-only**, and the result is exposed with the get method.

The **Substitution** class has three different substitutions **(M,P,T)**.  
**M**, **P** and **T** are three classes with different substitution expressions and they are executed depending on validation of the input parameters.

The **Custom_1** class extends Substitution and the **P** is replaced whit **P2**.  
**P2** is an extension of **P** and it overrides its core with a new expression. 

The **Custom_2** class extends the Substitution class and it changes the core of the get method.

### Dependencies ###
* Node, Express, TypeScript, Joi, jasmine, supertest

### How do I get set up? ###

* Run Dev: npm run start:dev
* Run Tests: npm run test
* Run npm run deploy
