import QueryString = require("qs");

const stringToBool = (str: string | QueryString.ParsedQs | string[] | QueryString.ParsedQs[] | undefined) => str == 'true';
const stringToNumber = (str: string | QueryString.ParsedQs | string[] | QueryString.ParsedQs[] | undefined) => Number(str);

export {stringToBool, stringToNumber};