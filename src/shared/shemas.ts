const Joi = require('joi');

const baseShema = Joi.object().keys({
    A: Joi.boolean().required(),
    B: Joi.boolean().required(),
    C: Joi.boolean().required(),
    D: Joi.string().regex(/[0-9][.][0-9]{1,100}$/).required(),
    E: Joi.number().integer().required(),
    F: Joi.number().integer().required(),
})

export default baseShema;
