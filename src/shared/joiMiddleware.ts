import { Request, Response, NextFunction } from 'express';
import { BAD_REQUEST } from 'http-status-codes';
import customJoiError from 'custom-joi-error';

const joiMiddleware = (schema: any, options:object) => {
    return (req:Request, res:Response, next:NextFunction) => {
        const result = schema.validate(req.query, options)
        if (result.error) {
            let errors = customJoiError(result, {
              language: {
                'D': {'string.pattern.base': 'D must be a Decimal number'}
              }
            });
            const { details } = errors;
            const messages = details.map((i:any) => i.message);
            res.status(BAD_REQUEST).json({
                errors: messages
            })
        } else {
            next();
        }
    }
}
export default joiMiddleware;