import StatusCodes, { BAD_REQUEST, OK } from 'http-status-codes';
import { Request, Response, Router } from 'express';
import Substitution from '../models/substitution_model';
import { stringToBool, stringToNumber } from '../shared/utils';
import Custom_1 from '../models/custom_1_model';
import Custom_2 from '../models/custom_2_model';
import  joiMiddleware from '../shared/joiMiddleware';
import baseShema from '../shared/shemas'

const router = Router();

router.get('/substitution', joiMiddleware(baseShema, {abortEarly: false}), async (req: Request, res: Response) => {
    try {
        const {A, B, C, D, E, F } = req.query;
        const substitution = await new Substitution(stringToBool(A), stringToBool(B), stringToBool(C), stringToNumber(D), stringToNumber(E), stringToNumber(F));
        return res.status(OK).json({data: substitution.H});
    } catch (error) {
        return res.status(BAD_REQUEST).json({error: error.message});
    }
});

router.get('/custom_1', joiMiddleware(baseShema, {abortEarly: false}), async (req: Request, res: Response) => {
    try {
        const {A, B, C, D, E, F } = req.query;
        const custom = await new Custom_1(stringToBool(A), stringToBool(B), stringToBool(C), stringToNumber(D), stringToNumber(E), stringToNumber(F));
        return res.status(OK).json({data: custom.H});
    } catch (error) {
        return res.status(BAD_REQUEST).json({error: error.message});
    }
});

router.get('/custom_2', joiMiddleware(baseShema, {abortEarly: false}), async (req: Request, res: Response) => {
    try {
        const {A, B, C, D, E, F } = req.query;
        const custom = await new Custom_2(stringToBool(A), stringToBool(B), stringToBool(C), stringToNumber(D), stringToNumber(E), stringToNumber(F));
        return res.status(OK).json({data: custom.H});
    } catch (error) {
        return res.status(BAD_REQUEST).json({error: error.message});
    }
});

export default router;
