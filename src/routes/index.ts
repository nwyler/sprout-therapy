import { Router } from 'express';
import Substitutions from './substitutions_routes';

// Init router and path
const router = Router();

// Add sub-routes
router.use('/', Substitutions);

// Export the base-router
export default router;
