import { P2 } from './shared_model';
import Substitution from './substitution_model';

class Custom_1 extends Substitution {
    constructor(
      A: boolean,
      B: boolean,
      C: boolean,
      D: number,
      E: number,
      F: number
    ) {
      super(A, B, C, D, E, F);
      if (A && B && C) this._H = new P2(D, E, F).K;
    }
  }

export default Custom_1;