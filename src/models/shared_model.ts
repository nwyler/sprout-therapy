class M {
    public K: number;
    constructor(D: number, E: number) {
        this.K = D + (D * E) / 10;
    }
}

class P {
    public K: number;
    constructor(D: number, E: number, F: number) {
        this.K = D + (D * (E - F)) / 25.5;
    }
}

class T {
    public K: number;
    constructor(D: number, F: number) {
        this.K = D - (D * F) / 30;
    }
}

// OVERRIDES
class P2 extends P {
    constructor(D: number, E: number, F: number) {
        super(D, E, F);
        this.K = 2 * D + (D * E) / 100;
    }
}

export { M, P, T, P2 }