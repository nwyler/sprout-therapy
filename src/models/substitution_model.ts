import { M, P, T } from './shared_model';

class Substitution {
    protected _H: number;
    protected A:boolean;
    protected B:boolean;
    protected C:boolean;
    protected D:number;
    protected E:number;
    protected F:number;

    constructor(
        A: boolean,
        B: boolean,
        C: boolean,
        D: number,
        E: number,
        F: number
    ) {
        this.A = A;
        this.B=  B;
        this.C = C;
        this.D = D;
        this.E = E;
        this.F = F;
        this._H = 0;
    }

    public get H() : number {
        if (this.A && this.B && !this.C) this._H = new M(this.D, this.E).K;
        else if (this.A && this.B && this.C) this._H = new P(this.D, this.E, this.F).K;
        else if (!this.A && this.B && this.C) this._H = new T(this.D, this.F).K;
        else throw new Error("B and one of (A or C) to be true");
        return Number(this._H.toFixed(2));
    }
}

export default Substitution;