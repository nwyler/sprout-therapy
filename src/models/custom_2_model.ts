import { M, T } from './shared_model';
import Substitution from './substitution_model';

class Custom_2 extends Substitution {

  constructor(
    A: boolean,
    B: boolean,
    C: boolean,
    D: number,
    E: number,
    F: number
  ) {
    super(A, B, C, D, E, F);
  }

  get H():number{
    if (this.A && this.B && !this.C) this._H = new T(this.D, this.E).K;
    else if (this.A && !this.B && this.C) this._H = new M(this.D, this.E).K;
    else this._H = this.F + this.D + (this.D * this.E) / 100;
    return Number(this._H.toFixed(2));
  }

}

export default Custom_2;